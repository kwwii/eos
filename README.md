# EOS project page

### Installation

1. `git clone git@gitlab.com:SUSE-UIUX/eos.git`
2. `cd eos`
3. `npm install`
4. `npm start`
5. visit: http://localhost:3000/

### Running lints:

Sass:
`npm run test:sass`

JS:
`npm run test:js`
