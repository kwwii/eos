var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')

var routes = require('./routes/index')
var iconsWhenToUseThem = require('./routes/icons/when-to-use-them')
var specialIconsCaasp = require('./routes/internal/special-icons/caasp')
var specialIconsCloud = require('./routes/internal/special-icons/cloud')
var specialIconsHawk = require('./routes/internal/special-icons/hawk')
var specialIconsPaas = require('./routes/internal/special-icons/paas')
var specialIconsScc = require('./routes/internal/special-icons/scc')
var specialIconsSes = require('./routes/internal/special-icons/ses')
var specialIconsSuma = require('./routes/internal/special-icons/suma')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))

app.use('/', routes)
app.use('/icons/when-to-use-them', iconsWhenToUseThem)
app.use('/internal/special-icons/caasp', specialIconsCaasp)
app.use('/internal/special-icons/cloud', specialIconsCloud)
app.use('/internal/special-icons/hawk', specialIconsHawk)
app.use('/internal/special-icons/paas', specialIconsPaas)
app.use('/internal/special-icons/scc', specialIconsScc)
app.use('/internal/special-icons/ses', specialIconsSes)
app.use('/internal/special-icons/suma', specialIconsSuma)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
