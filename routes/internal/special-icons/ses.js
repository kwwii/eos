var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('internal/special-icons/ses', { title: 'Special icons: SUSE Enterprise Storage', path: req.originalUrl })
})

module.exports = router
