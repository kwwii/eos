var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('internal/special-icons/cloud', { title: 'Special icons: SUSE Cloud', path: req.originalUrl })
})

module.exports = router
