var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('internal/special-icons/scc', { title: 'Special icons: SCC', path: req.originalUrl })
})

module.exports = router
