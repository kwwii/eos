var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('internal/special-icons/paas', { title: 'Special icons: PaaS', path: req.originalUrl })
})

module.exports = router
