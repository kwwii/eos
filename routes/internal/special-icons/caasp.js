var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('internal/special-icons/caasp', { title: 'Special icons: CaaSP', path: req.originalUrl })
})

module.exports = router
