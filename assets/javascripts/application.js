$(document).on('ready', function () {
  // print the class of fontawesome icon in a label
  $('.fa').each(function () {
    var classes = $(this).attr('class')
    $(this).parent().append('<span class=\'label label-default\'>' + classes + '</span>')
  })

  // count how many icons are displayed on the page for the special icons lists
  var currentPath = window.location.pathname
  var iconsPath = '/internal/special-icons/'
  var isInIconsPath = currentPath.indexOf(iconsPath) !== -1

  if (isInIconsPath) {
    var $counterContainer = $('.js-count-icons')
    var $iconsContainer = $('.icons-list article')
    var countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  }
})
